# Gulp Basic Setup

![Gulp](https://raw.github.com/gulpjs/artwork/master/gulp.png)

This is a basic Gulp setup. The `gulpfile.js` will do the following things:

1. Retrieve dependencies
2. Define paths
3. Process Sass files `./src/styles/`
4. Process JS script files in `./src/scripts/` en libraries in `./src/scripts/libs`
5. Process Jade files in `./src/templates/`
6. Process HTML files in `./src/`
7. Process image files in `.src/images/`
8. Clean up the `./dst/`
9. Create a server on `http://localhost:8080` and watch for changes
10. Define default Gulp task

It also lints the scripts, concatinates and minifies scripts into a single file and libraries into another. It processes and minifies Jade and HTML.

In order to run this, you will need [NodeJS](http://nodejs.org/) and [Gulp](http://gulpjs.com/). 

## Installation

CD to your local project
~~~
cd ~/dev/project
~~~
Clone the repo
~~~
git clone https://kojinga@bitbucket.org/kojinga/gulp-basic-setup.git
~~~
Install the dependencies defined in `package.json`
~~~
npm install
~~~
Gulp!
~~~
gulp
~~~

To add individual packages, use `npm install --save-dev gulp-packagename` and add require them in your `gulpfile.js`.

---
## Directory Structure

~~~
./src                     // source dir
./../scripts              // scripts dir
./../../.js               // application specific JavaScripts 
./../../libs              // dir for vendor specific JavaScripts 
./../../../.js            // individual JavaScript files 
./../styles               // dir for pre-processed stylesheets 
./../../.scss             // individual SASS files 
./../images               // dir for uncompressed images 
./../../.png              // individual images 
./../templates            // dir for HTML templates 
./../../.jade             // individual Jade templates 
./gulpfile.js             // configures Gulp tasks 
./package.json            // defines dependencies 
./readme.md               // the file you're reading right now 
~~~

## Output Directory Structure

~~~
./dst                     // output dir
./../scripts              // scripts dir
./../../all.min.js        // minified application specific JavaScripts 
./../../libraries.min.js  // minified JavaScript files 
./../styles               // dir for minified stylesheets 
./../../.css              // individual processed CSS files 
./../images               // dir for compressed images 
./../../.png              // individual images 
./../index.html           // Jade generated HTML web pages 
~~~

## Gulp Plugins Defined in `package.json`
~~~
gulp                      // gulp core
gulp-sass                 // compile SASS stylesheets 
gulp-jade                 // compile Jade templates 
gulp-watch                // an endless stream 
gulp-clean                // for removing files and folders 
gulp-autoprefixer         // automatically prefix files 
gulp-minify-css           // minify CSS files 
gulp-htmlmin              // minify HTML files 
gulp-imagemin             // minify images 
gulp-uglify               // minify JS files 
gulp-jshint               // check files with jshint
gulp-concat               // for joining files together
gulp-notify               // send a notification 
gulp-jshint-stylish       // beautify jshint checks
gulp-connect              // spawn a server
gulp-plumber              // keep things running
~~~

## Additional Useful Plugins

~~~
gulp-browserify           // bundle modules with Browserify 
gulp-changed              // only pass through changed files 
gulp-coffee               // compile CoffeeScript files 
gulp-coffeelint           // lint CoffeeScript files with CoffeeLint 
gulp-compass              // SASS Compass Compiler 
gulp-cssbeautify          // reindent and reformat CSS 
gulp-csslint              // lint CSS files 
gulp-filesize             // logs filesizes in human readable format 
gulp-gh-pages             // publish to GitHub pages 
gulp-git                  // Git plugin 
gulp-handlebars           // Handlebars 
gulp-html-prettify        // Prettify HTML templates 
gulp-ignore               // include or exclude files from the stream 
gulp-include-source       // automatically include scripts and styles 
gulp-less                 // compile Less files 
gulp-livereload           // reload server upon file changes 
gulp-markdown             // compile Markdown files 
gulp-open                 // open files and urls 
gulp-rename               // rename files 
gulp-stylus               // compile STYLUS stylesheets 
~~~

You can find this setup at [BitBucket](https://bitbucket.org/kojinga/gulp-basic-setup) and me on [@svdb](https://twitter.com/svdb).