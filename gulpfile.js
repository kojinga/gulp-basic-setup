// Name: Gulp Basic Setup
// Version: 0.1.0
// Author: @svdb
// Git: https://bitbucket.org/kojinga/gulp-basic-setup

// ----------------------------------------------------------------------------
// 1. Dependencies
// Require dependencies. Specific versions are defined in package.json.
// ----------------------------------------------------------------------------

'use strict';

var gulp         = require('gulp'),                     // gulp core
    jade         = require('gulp-jade'),                // jade compi;er
    sass         = require('gulp-sass'),                // sass compiler
    clean        = require('gulp-clean'),               // clean up dst dir
    watch        = require('gulp-watch'),               // watch for changes
    uglify       = require('gulp-uglify'),              // js uglifier
    jshint       = require('gulp-jshint'),              // js syntax checker
    concat       = require('gulp-concat'),              // join files
    notify       = require('gulp-notify'),              // notifications
    htmlmin      = require('gulp-htmlmin'),             // minify html
    plumber      = require('gulp-plumber'),             // keep alive
    stylish      = require('jshint-stylish'),           // make errors look good in shell
    connect      = require('gulp-connect'),             // connect to server
    imagemin     = require('gulp-imagemin'),            // minify images
    minifycss    = require('gulp-minify-css'),          // minify the css files
    autoprefixer = require('gulp-autoprefixer');        // sets missing browserprefixes

// ----------------------------------------------------------------------------
// 2. Paths
// Input and output dirs defined first. Then their subdirs.
// ----------------------------------------------------------------------------

var path_src = './src/';                                // path_src
var path_dst = './dst/';                                // path_dst

var src = {
    html      : path_src + '*.html',                    // src.html
    templates : path_src + 'templates/**/*.jade',       // src.templates
    libraries : path_src + 'scripts/libs/**/*.js',      // src.libraries
    scripts   :[path_src + 'scripts/*.js',              // src.scripts
          '!' + path_src + 'scripts/libs/**/*.js'],     // exclude libs
    styles    : path_src + 'styles/**/*.scss',          // src.styles
    images    : path_src + 'images/**/*',
    extras    :[path_src + 'humans.txt',                // src.extras
                path_src + 'robots.txt',
                path_src + 'favicon.ico']
};

var dst = {
    templates : path_dst,                               // dst.templates
    scripts   : path_dst + 'scripts/',                  // dst.scripts
    styles    : path_dst + 'styles/',                   // dst.styles
    images    : path_dst + 'images/',                   // dst.images
    extras    :[path_dst + 'humans.txt',                // dst.extras
                path_dst + 'robots.txt',
                path_dst + 'favicon.ico']
};

// ----------------------------------------------------------------------------
// 3. SASS
// From src.styles to dst.styles
// Plumbing, processing, prefixing, minifying and error checking
// ----------------------------------------------------------------------------

gulp.task('sass', function() {
    gulp.src(src.styles)                                // input from styles
        .pipe(plumber())                                // keep alive on errors
        .pipe(sass())                                   // compile sass
        .pipe(autoprefixer(                             // vendor prefixes
            'last 2 version',
            '> 1%',
            'ie 8',
            'ie 9',
            'ios 6',
            'android 4'
        ))
        .pipe(minifycss())                              // minify css
        .pipe(gulp.dest(dst.styles))                    // output to dst
        .on('error', notify.onError({                   // notify of errors
            message: '<%= error.message %>',
            title: 'SASS Error'
        }))
        .pipe(notify({message: 'SCSS processed!'}));    // notify done
});

// ----------------------------------------------------------------------------
// 4. JavaScript
// From src.scripts to dst.scripts and from src.libraries to dst.scripts
// Scripts and libraries: Hinting, uglifying, concatifying and error checking
// ----------------------------------------------------------------------------

gulp.task('js-other', function() {
    gulp.src(src.scripts)                               // input from scripts
        .pipe(jshint())                                 // lint the files
        .pipe(jshint.reporter(stylish))                 // readable linting 
        .pipe(uglify())                                 // uglification
        .pipe(concat('all.min.js'))                     // concat into 1 file
        .pipe(gulp.dest(dst.scripts))                   // output to dst
        .on('error', notify.onError({                   // notify of errors
            message: '<%= error.message %>',
            title: 'JS Error'
        }))
        .pipe(notify({message: 'JS scripts processed!'}));   // notify done
});

gulp.task('js-libraries', function() {
    gulp.src(src.libraries)                             // input from libraries
        .pipe(uglify())                                 // uglification
        .pipe(concat('libraries.min.js'))               // concat into 1 file
        .pipe(gulp.dest(dst.scripts))                   // output to dst
        .on('error', notify.onError({                   // notify of errors
            message: '<%= error.message %>',
            title: 'JS Error'
        }))
        .pipe(notify({message: 'JS libraries processed!'})); // notify done
});

// ----------------------------------------------------------------------------
// 5. Jade
// From src.templates to dst.templates
// Processing, minifying and error checking
// ----------------------------------------------------------------------------

gulp.task('jade', function() {
    return gulp.src(src.templates)                      // input from templates
    .pipe(jade())                                       // process jade
    .pipe(htmlmin())                                    // minify html
    .pipe(gulp.dest(dst.templates))                     // output to dst
    .on('error', notify.onError({                       // notify of errors
        message: '<%= error.message %>',
        title: 'Jade Error'
    }))
    .pipe(notify({message: 'Jade processed!' }));       // notify done
});

// ----------------------------------------------------------------------------
// 6. HTML
// From src.html to dst.templates
// Processing, minifying and error checking
// ----------------------------------------------------------------------------

gulp.task('html', function() {
    return gulp.src(src.html)                           // input from html
    .pipe(htmlmin())                                    // minify html
    .pipe(gulp.dest(dst.templates))                     // output to dst
    .on('error', notify.onError({                       // notify on errors
        message: '<%= error.message %>',
        title: 'HTML Error'
    }))
    .pipe(notify({message: 'HTML processed!' }));       // notify done
});

// ----------------------------------------------------------------------------
// 7. Images
// Minify and optimize images
// ----------------------------------------------------------------------------

gulp.task('images', function() {
  return gulp.src(src.images)                           // input from images
    .pipe(imagemin({optimizationLevel: 4}))             // minify images
    .pipe(gulp.dest(dst.images))                        // output to dst
    .on('error', notify.onError({                       // notify on errors
        message: '<%= error.message %>',
        title: 'Image Error'
    }))
    .pipe(notify({message: 'Images processed!' }));     // notify done
});

// ----------------------------------------------------------------------------
// 8. Clean
// Remove everything in the dst dir
// ----------------------------------------------------------------------------

gulp.task('clean', function() {
  return gulp.src([path_dst + '**/*.*'], {read: false}) // input from dst
    .pipe(clean())                                      // delete contents
    .pipe(notify({message: 'DST cleaned!' }));          // notify done
});

// ----------------------------------------------------------------------------
// 9. Watch Server
// Open up a server on localhost:8080 and watch for changes
// ----------------------------------------------------------------------------

gulp.task('serve', function() {
    connect.server({                                    // connect to server
        root: path_dst,                                 // root dir
        port: 8080,                                     // port
        livereload: true                                // livereload on
    });

    watch({glob: path_dst + '**/*.*'})                  // watch dir                  
        .pipe(connect.reload());                        // reload on change
});

// ----------------------------------------------------------------------------
// 10. Gulp Tasks
// Default task definitions
// ----------------------------------------------------------------------------

gulp.task('default', [                                  // default task
    'html',                                             // process html
    'jade',                                             // process jade
    'sass',                                             // process sass
    'js-other',                                         // process scripts
    'js-libraries',                                     // process libraries
    'images',                                           // process images
    'serve',                                            // create a server
    'watch'                                             // watch for changes
]);

gulp.task('watch', function() {
    gulp.watch(src.html, ['html']);
    gulp.watch(src.templates, ['jade']);
    gulp.watch(src.styles, ['sass']);
    gulp.watch(src.scripts, ['js-other']);
    gulp.watch(src.scripts, ['js-libraries']);
    gulp.watch(src.images, ['images']);
});